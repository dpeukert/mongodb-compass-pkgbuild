# PKGBUILDs

![My artistic masterpiece representing this repo](https://gitlab.com/uploads/-/system/project/avatar/7249074/pkgbuilds.png){width=250px height=250px}

This repo contains PKGBUILD files for my AUR packages and is managed using [salvador](https://gitlab.com/dpeukert/salvador).

## Copyright/attribution info

### Electron launcher scripts (`{beekeper-studio,electron-*-bin,expresslrs-configurator}/electron-launcher.sh`)

Based on the [electron-launcher.sh script from the electron package in the repos](https://gitlab.archlinux.org/archlinux/packaging/packages/electron32/-/blob/main/electron-launcher.sh) originally presumed ([\[1\]](https://bbs.archlinux.org/viewtopic.php?id=200786) [\[2\]](https://bbs.archlinux.org/viewtopic.php?id=252234)) to be released into the public domain and released into the public domain or where not possible, licensed under the [Unlicense](LICENSE.UNLICENSE).

### .gitignore files (`{,_common}/.gitignore`)

Based on the [ArchLinuxPackages.gitignore file in GitHub's gitignore repo](https://github.com/github/gitignore/blob/main/ArchLinuxPackages.gitignore) originally licensed under [CC0](https://github.com/github/gitignore/blob/main/LICENSE) and released into the public domain or where not possible, licensed under the [Unlicense](LICENSE.UNLICENSE).

### light patches (`light/*.diff`)

Representing commits from the [perkele1989/light repo](https://web.archive.org/web/20231226103420/https://github.com/perkele1989/light), which no longer exists, licensed under [GPLv3](https://gitlab.com/dpeukert/light/-/blob/main/COPYING).

### weatherspect man page (`weatherspect/weatherspect.1`)

Based on the [weatherspect README](https://github.com/AnotherFoxGuy/weatherspect/blob/master/README.md) originally licensed under [GPLv2](https://github.com/AnotherFoxGuy/weatherspect/blob/master/gpl.txt) (changes: transformed the Markdown readme into the man page format, date: 2019/12/30), therefore licensed under [GPLv2](LICENSE.GPLv2) as well.

### wxtoimg-beta string patching logic (part of `wxtoimg-beta/PKGBUILD`)

Based on a [blogpost from Johan Hedin](https://everydaywithlinux.blogspot.com/2012/11/patch-strings-in-binary-files-with-sed.html) with no explicit license, but based on the fact that it's been publicly shared with an encouragement to use it, this attribution and a thanks is provided. If you're Johan Hedin, feel free to reach out to either license the code explicitly or request its removal.

### All other files

Released into the public domain or where not possible, licensed under the [Unlicense](LICENSE.UNLICENSE). This presumes ([\[1\]](https://bbs.archlinux.org/viewtopic.php?id=200786) [\[2\]](https://bbs.archlinux.org/viewtopic.php?id=252234)) previous contributions to PKGBUILDs (if present) to be released into the public domain.
